import QtQuick 2.9
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3
import Lomiri.Content 1.3
import Lomiri.Components.Popups 1.3
import Qt.labs.settings 1.0
import QtWebEngine 1.7
import "../components"

Page {
    id: page
    width: parent.width
    height: parent.height
    property var contextMenuRequest: null
    property var instance: settings.instance;

    header: Rectangle {
        color: LomiriColors.orange
        width: parent.width
        height: units.gu(0)
    }

    ContentHubModel {
        id: contentHubModel
    }

    Settings {
        id: settings
        property var instance
    }

    Component {
        id: pickerComponent
        PickerDialog {}
    }

    property var transfer:  null

    Connections {
        target: ContentHub
        onShareRequested: {
            if ( transfer.contentType === ContentType.Links || transfer.contentType === ContentType.Text ) {
                var message = "";
                for ( var i = 0; i < transfer.items.length; i++ ) {
                    if (String(transfer.items[i].text).length > 0 && String(transfer.items[i].url).length == 0) {
                        message += String(transfer.items[i].text)
                    }
                    else if (String(transfer.items[i].url).length > 0 ) {
                        message += String(transfer.items[i].url)
                    }
                    if ( i+1 < transfer.items.length ) message += "\n"
                }
                webView.url = (instance.indexOf("http") != -1 ? instance : "https://" + instance) + "/share?text=" + message
            }
        }
    }

    WebEngineView {
        id: webView
        width: parent.width
        height: parent.height
        visible: false
        onLoadProgressChanged: {
            progressBar.value = loadProgress
            if ( loadProgress === 100 ) visible = true
        }
        anchors.fill: parent
        url: instance.indexOf("http") != -1 ? instance : "https://" + instance
	    zoomFactor: units.gu(1) / 8

        ErrorSheet {
            z: 100
            visible: webView.status === WebEngineView.LoadFailedStatus
            onRefreshClicked: webView.reload()
        }


        onFileDialogRequested: {
			request.accepted = true;
			var fakeModel = {
					allowMultipleFiles: request.mode == FileDialogRequest.FileModeOpenMultiple,
					reject: function() {
						request.dialogReject();
					},
					accept: function(files) {
							request.dialogAccept(files);
					}
			};
			var  pickerInstance = pickerComponent.createObject(webView,{model:fakeModel});
	    }
        Component {
            id: contextualMenu
            Popover {
                ActionBar {
                    anchors.centerIn: parent
                    anchors.topMargin: units.gu(1)
                    anchors.bottomMargin: units.gu(1)
                    numberOfSlots: 10
                    delegate:  Rectangle {
                        color: mousearea.pressed ? "#CCCCCC" : "#FFFFFF"
                        width: units.gu(8)
                        height: Math.max(units.gu(4), implicitWidth) + units.gu(2)
                        visible: modelData.enabled
                        Icon {
                            id: icon
                            width: units.gu(2.5)
                            height: width
                            name: modelData.iconName
                            anchors.top: parent.top
                            anchors.topMargin: units.gu(0.5)
                            anchors.horizontalCenter: parent.horizontalCenter
                        }
                        Label {
                            id: labelText
                            text: modelData.text
                            textSize: Label.XSmall
                            width: parent.width - units.gu(3)
                            wrapMode: Text.NoWrap
                            horizontalAlignment: Text.AlignHCenter
                            anchors.top: icon.bottom
                            anchors.topMargin: units.gu(0.5)
                            anchors.horizontalCenter: parent.horizontalCenter
                        }
                        MouseArea {
                            id: mousearea
                            anchors.fill: parent
                            onClicked: modelData.triggered(null)
                        }
                    }

                    actions: [
                        Action {
                            text: i18n.tr("Open in browser")
                            iconName: "external-link"
                            enabled: !contextMenuRequest.isContentEditable && contextMenuRequest.selectedText === "" && (contextMenuRequest.mediaUrl !== "" || contextMenuRequest.linkText !== "")
                            onTriggered: Qt.openUrlExternally( contextMenuRequest.mediaUrl !== "" ? contextMenuRequest.mediaUrl : contextMenuRequest.linkText )
                        },
                        Action {
                            iconName: "share"
                            text: i18n.tr("Share")
                            enabled: !contextMenuRequest.isContentEditable && (contextMenuRequest.mediaUrl !== "" || contextMenuRequest.linkUrl !== "")
                            onTriggered: contentHubModel.shareLink(contextMenuRequest.mediaUrl !== "" ? contextMenuRequest.mediaUrl : contextMenuRequest.linkUrl)
                        },
                        Action {
                            iconName: "edit-select-all"
                            text: i18n.tr("Select all")
                            enabled: contextMenuRequest.mediaType === ContextMenuRequest.MediaTypeNone
                            onTriggered: webView.triggerWebAction(WebEngineView.SelectAll)
                        },
                        Action {
                            text: i18n.tr("Copy url")
                            iconName: "stock_image"
                            enabled: contextMenuRequest.mediaType !== ContextMenuRequest.MediaTypeNone && contextMenuRequest.selectedText !== ""
                            onTriggered: Clipboard.push([contextMenuRequest.mediaUrl])
                        },
                        Action {
                            text: i18n.tr("Paste")
                            iconName: "edit-paste"
                            enabled: contextMenuRequest.isContentEditable
                            onTriggered: webView.triggerWebAction(WebEngineView.Paste)
                        },
                        Action {
                            text: i18n.tr("Paste")
                            iconName: "edit-cut"
                            enabled: contextMenuRequest.isContentEditable
                            onTriggered: webView.triggerWebAction(WebEngineView.Cut)
                        },
                        Action {
                            text: i18n.tr("Copy")
                            iconName: "edit-copy"
                            enabled: contextMenuRequest.mediaType === ContextMenuRequest.MediaTypeNone && (contextMenuRequest.selectedText !== "" || contextMenuRequest.linkUrl !== "")
                            onTriggered: webView.triggerWebAction(WebEngineView.Copy)
                        }
                    ]
                }
            }
        }

        onContextMenuRequested: {
            console.log("Context menu requested");
            request.accepted = true;
            contextMenuRequest = request;
            PopupUtils.open(contextualMenu);
        }
    
        // Open external URL's in the browser and not in the app
        onNavigationRequested: {
            console.log ( request.url, ("" + request.url).indexOf ( instance ) !== -1 )
            if ( ("" + request.url).indexOf ( instance ) !== -1 ) {
                request.action = 0
            } else {
                request.action = 1
                Qt.openUrlExternally( request.url )
            }
        }
        onNewViewRequested: {
            request.action = 1
            Qt.openUrlExternally( request.requestedUrl )
        }
    }

    Rectangle {
        anchors.fill: parent
        visible: !webView.visible
        color: "#191b22"

        Label {
            id: progressLabel
            color: "white"
            text: i18n.tr('Loading ') + instance
            anchors.centerIn: parent
            textSize: Label.XLarge
        }

        ProgressBar {
            id: progressBar
            value: 0
            minimumValue: 0
            maximumValue: 100
            anchors.top: progressLabel.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 10
        }

        Button {
            anchors.bottom: parent.bottom
            anchors.bottomMargin: height
            anchors.horizontalCenter: parent.horizontalCenter
            color: LomiriColors.red
            text: i18n.tr("Choose another Instance")
            onClicked: {
                mainStack.clear ()
                mainStack.push (Qt.resolvedUrl("./InstancePicker.qml"))
                instance = undefined
            }
        }
    }
}
